/* eslint-disable prettier/prettier */
import AdminJS from 'adminjs';
import { Module } from "@nestjs/common";
import { TypeOrmModule } from "@nestjs/typeorm";
import { AdminModule } from "@adminjs/nestjs";
import { Database, Resource } from '@adminjs/typeorm'

import { AppController } from "./app.controller";
import { AppService } from "./app.service";
import { UserModule } from "./modules/user";
import { AuthModule } from "./modules/auth";
import { User } from "./modules/user/user.entity";
import { userMeta } from "./modules/user/usermeta.entity";

AdminJS.registerAdapter({ Database, Resource })

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: "mysql",
      host: "localhost",
      port: 3306,
      username: "root",
      password: "",
      database: "task_cms",
      entities: [User, userMeta],
      synchronize: true,
    }),
    AdminModule.createAdmin({
      adminJsOptions: {
        rootPath: "/admin",
        resources: [User, userMeta],
      },
    }),
    UserModule,
    AuthModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
