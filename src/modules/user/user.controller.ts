/* eslint-disable prettier/prettier */
import { ClassSerializerInterceptor, UseInterceptors, Controller, Get, Param} from '@nestjs/common';
import { UserService } from './user.service';
import { User } from './user.entity';

@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService){}

  @UseInterceptors(ClassSerializerInterceptor)
  @Get(':id')
  async findAll(@Param('id') id: number): Promise<User> {
    const user = await this.userService.getUser(id);
    return user;
  }
}
