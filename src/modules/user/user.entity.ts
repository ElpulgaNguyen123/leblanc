/* eslint-disable prettier/prettier */
import { Entity, Column, PrimaryGeneratedColumn, OneToMany, BaseEntity } from 'typeorm';
import { Exclude } from 'class-transformer';
import {userMeta} from './usermeta.entity';

@Entity('task_users', {schema : 'task_cms'})

class User extends BaseEntity{
  constructor(partial: Partial<User>) {
    super()
    Object.assign(this, partial);
  }
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 50 })
  user_login: string;

  @Exclude()
  @Column({ length: 50 })
  user_pass: string;

  @Column({ length: 50 })
  user_name: string;

  @Column({ length: 50 })
  user_email: string;

  @Column({ length: 50 })
  display_name: string;

  @Column({ length: 50 })
  user_status: string;

  @Column({ length: 50 })
  avatar: string;

  @Column({ length: 50 })
  something: string;

  @OneToMany(() => userMeta, usermeta => usermeta.user_id)
  userMetas : userMeta[]
}

export {User}
