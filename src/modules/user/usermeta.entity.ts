/* eslint-disable prettier/prettier */
import {Entity ,Column, PrimaryGeneratedColumn, BaseEntity} from 'typeorm';
import {User} from './user.entity';

@Entity('task_usermeta', {schema: 'task_cms'})

class userMeta extends BaseEntity {

    @PrimaryGeneratedColumn()
    umeta_id? : number;

    @Column()
    user_id : number;

    @Column({ length: 200 })
    meta_key : string;  

    @Column({ length: 200 })
    meta_value : string;
}

export {userMeta}