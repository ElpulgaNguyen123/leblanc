/* eslint-disable prettier/prettier */
import { Injectable} from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { User } from "./user.entity";
import { userMeta } from "./usermeta.entity";

@Injectable()
export class UserService {
  
  constructor(
    @InjectRepository(User)
    private usersRepository: Repository<User>,
    @InjectRepository(userMeta)
    private userMetasRepository: Repository<userMeta>,
  ) {}

  async getUser(id : number) : Promise<User> {
    const user = await this.usersRepository.findOneBy({id});
    return user;
  }

  async addUserMeta(usermeta : userMeta) : Promise<userMeta>{
    return await this.userMetasRepository.create(usermeta);
  }

  async findOne(userName : string) : Promise<User>{
    const user = await this.usersRepository.findOneBy({user_email : userName})
    return user;
  }
}
    