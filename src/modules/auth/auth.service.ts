/* eslint-disable prettier/prettier */

import { Injectable } from "@nestjs/common";
import { UserService } from "../user/user.service";
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {

  constructor(
    private usersService: UserService, 
    private jwtService: JwtService) {}

  async validateUser(username: string, password: string): Promise<any> {
    const user = await this.usersService.findOne(username);
    if(user && password == user.user_pass){
      return user;
    }
    return null;
  }

  async login(user: any) {
    const payload = { username: user.user_name, sub: user.id };
    console.log('payload', payload);
    return {
      access_token: this.jwtService.sign(payload),
    };
  }
  
}
